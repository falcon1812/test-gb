<?php

namespace App\Http\Controllers;

use App\Exceptions\StudioClassException;
use App\Services\StudioClassService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @SWG\Tag(
 *   name="Classes",
 *   description="Classes and stuff related with"
 * )
 */
class StudioClassController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/class",
     *     @SWG\Parameter(
     *        name="body",
     *        in="body",
     *        description="Class properties",
     *        required=true,
     *        @SWG\Property(type="object",
     *             @SWG\Property(property="class_time", type="string"),
     *             @SWG\Property(property="start_date", type="string"),
     *             @SWG\Property(property="end_date", type="string"),
     *             @SWG\Property(property="studio_id", type="integer")
     *        ),
     *      ),
     *     @SWG\Response(
     *          response="200",
     *          description="Returns itself as an object (StudioClassEntity)",
     *          @SWG\Schema(ref="#/definitions/StudioClassEntity")),
     *     tags={"Classes"},
     * )
     */
    public function createClass(StudioClassService $classService, Request $request): JsonResponse
    {
        $classBody = $request->all();

        if (empty($classBody)) {
            StudioClassException::bodyMissing();
        }

        return response()->json(['StudioClass' => $classService->createClass($classBody)]);
    }
}