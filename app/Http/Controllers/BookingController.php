<?php

namespace App\Http\Controllers;

use App\Exceptions\BookingException;
use App\Services\BookingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @SWG\Tag(
 *   name="Bookings",
 *   description="Bookings and stuff related with"
 * )
 */
class BookingController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/booking",
     *     @SWG\Parameter(
     *        name="body",
     *        in="body",
     *        description="Booking properties",
     *        required=true,
     *        @SWG\Property(type="object",
     *             @SWG\Property(property="user_name", type="string"),
     *             @SWG\Property(property="time", type="string"),
     *             @SWG\Property(property="class_id", type="integer"),
     *             @SWG\Property(property="studio_id", type="integer")
     *        ),
     *      ),
     *     @SWG\Response(
     *          response="200",
     *          description="Returns itself as an object (BookingEntity)",
     *          @SWG\Schema(ref="#/definitions/BookingEntity")),
     *     tags={"Bookings"},
     * )
     */
    public function createBooking(BookingService $bookingService, Request $request): JsonResponse
    {
        // bookingServices is been bind on app/Providers/AppServiceProvider.php:register()
        $bookingBody = $request->all();

        if (empty($bookingBody)) {
            BookingException::bodyMissing();
        }

        return response()->json($bookingService->createBooking($bookingBody));
    }
}