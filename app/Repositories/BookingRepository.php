<?php

namespace App\Repositories;

use App\Entities\Booking;

class BookingRepository extends Repository
{
    /**
     * @var Booking
     */
    private $booking;

    /**
     * BookingRepository constructor.
     */
    public function __construct()
    {
        $this->setBooking(new Booking);

        parent::__construct(); // standards . . .
    }

    /**
     * @return Booking
     */
    public function getBooking(): Booking
    {
        return $this->booking;
    }

    /**
     * @param Booking $booking
     */
    public function setBooking(Booking $booking): void
    {
        $this->booking = $booking;
    }

    /**
     * @param array $booking
     * @return mixed
     */
    public function createBooking(array $booking)
    {
        return $this->getBooking()->create($booking);
    }
}