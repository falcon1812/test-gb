<?php

namespace App\Repositories;

use App\Entities\StudioClass;

class StudioClassRepository extends Repository
{
    /**
     * @var StudioClass
     */
    private $studioClass;

    /**
     * StudioClassRepository constructor.
     */
    public function __construct()
    {
        $this->setStudioClass(new StudioClass);

        parent::__construct();
    }

    /**
     * @param array $studioArray
     * @return mixed
     */
    public function createStudioClass(array $studioArray)
    {
        return $this->getStudioClass()->create($studioArray); // I'll prefer a DTO here tho
    }

    /**
     * @return StudioClass
     */
    protected function getStudioClass(): StudioClass
    {
        return $this->studioClass;
    }

    /**
     * @param StudioClass $studioClass
     */
    protected function setStudioClass(StudioClass $studioClass): void
    {
        $this->studioClass = $studioClass;
    }
}