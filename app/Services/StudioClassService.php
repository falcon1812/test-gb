<?php

namespace App\Services;

use App\Repositories\StudioClassRepository;

class StudioClassService extends Service
{
    /**
     * @var StudioClassRepository
     */
    private $classRepository;

    /**
     * StudioClassService constructor.
     */
    public function __construct()
    {
        $this->setClassRepository(new StudioClassRepository);

        parent::__construct();
    }

    /**
     * @return StudioClassRepository
     */
    public function getClassRepository()
    {
        return $this->classRepository;
    }

    /**
     * @param $classRepository StudioClassRepository
     */
    public function setClassRepository($classRepository): void
    {
        $this->classRepository = $classRepository;
    }

    /**
     * @return string
     */
    public function createClass(array $studioClass)
    {
        return $this->getClassRepository()->createStudioClass($studioClass);
    }
}