<?php

namespace App\Services;

use App\Repositories\BookingRepository;

class BookingService extends Service
{
    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * BookingService constructor.
     */
    public function __construct()
    {
        $this->setBookingRepository(new BookingRepository);

        parent::__construct();
    }

    /**
     * @param BookingRepository $bookingRepository
     */
    public function setBookingRepository(BookingRepository $bookingRepository): void
    {
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * @param array $booking
     * @return mixed
     */
    public function createBooking(array $booking)
    {
        return $this->bookingRepository->createBooking($booking);
    }
}