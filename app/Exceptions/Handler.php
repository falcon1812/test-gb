<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Changing this a bit so is more generic and always returns json
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $exception)
    {
        $message = $exception->getMessage();
        $code = $exception->getCode() == 0 || $exception->getCode() > 500 ? 500 : $exception->getCode();

        if (is_object($message)) {
            $message = $message->toArray();
        }

        $this->report($exception); // report exception

        return new JsonResponse(['error' => $message], $code);
    }
}
