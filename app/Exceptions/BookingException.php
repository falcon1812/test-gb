<?php

namespace App\Exceptions;

use Throwable;

class BookingException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function bodyMissing()
    {
        throw new BookingException('Missing body', 400); // all exceptions are handle in app/Exceptions/Handler.php:render()
    }
}