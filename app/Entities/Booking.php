<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes; // again, not sure if is necessary but you never know as well

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booking';

    /**
     * @SWG\Definition(
     *      definition="BookingEntity",
     *      @SWG\Property(property="Booking", type="object",
     *          @SWG\Property(property="id", type="integer"),
     *          @SWG\Property(property="studio_id", type="integer"),
     *          @SWG\Property(property="class_id", type="integer"),
     *          @SWG\Property(property="time", type="string"),
     *          @SWG\Property(property="user_name", type="string"),
     *          @SWG\Property(property="created_at", type="string"),
     *          @SWG\Property(property="updated_at", type="string"),
     *    ),
     * )
     */
    protected $fillable = [
        'studio_id',
        'class_id',
        'time',
        'user_name', // I'll prefer an userID but ¯\_(ツ)_/¯
    ];
    // I prefer getters and setters but this is laravel native
}