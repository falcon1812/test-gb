<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudioClass extends Model
{
    use SoftDeletes; // not sure if is necessary but you never know

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'studio_class';

    /**
     * @SWG\Definition(
     *      definition="StudioClassEntity",
     *      @SWG\Property(property="StudioClass", type="object",
     *          @SWG\Property(property="id", type="integer"),
     *          @SWG\Property(property="studio_id", type="integer"),
     *          @SWG\Property(property="class_time", type="string"),
     *          @SWG\Property(property="start_date", type="string"),
     *          @SWG\Property(property="end_date", type="string"),
     *          @SWG\Property(property="capacity", type="integer"),
     *          @SWG\Property(property="created_at", type="string"),
     *          @SWG\Property(property="updated_at", type="string"),
     *    ),
     * )
     */
    protected $fillable = [
        'studio_id',
        'class_name',
        'class_time',
        'start_date',
        'end_date',
        'capacity'
    ];

}