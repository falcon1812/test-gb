# Test-gf

Basic API test which is implementing the repository pattern [more info about it](https://share.nuclino.com/p/Repository-Pattern-v03E81eQHhgIYa9uhKU3RN)

We are using laravel 5.8 as framework

## Requirements

*  `PHP >= v7.2`
*  `Composer >= v1.8.6`
*  `Mysql >= 5.7` (in case of local use)

## Setup
*  First of all we need to create a `.env ` file which would contains everything related with the environment, for this you could simply copy `.env.example` as `.env`
*  Second we need to do a `composer install`, this should install every dependency 
*  For serving the project just run `php artisan serve`

## Documentation (Swagger :3)
*  By default on `.env` swagger is always generating the documents
*  Swagger can be found on `http://{domain}/api/documentation` 
*  Models for swagger are been define on the entities
*  Each endpoint has the response plus the model(s) is returning

## Error handler

All exceptions are been catch on `app/Exceptions/Handler.php:render()` for consistency, the `report()` method is meant to send every exception to a centralized logging tool like ELK or stackdriver

## Contribution

For adding more code or changing things please have a look at the commit standard defined, [Styleguides](https://share.nuclino.com/p/Styleguides-4bWkHjVM-YNcFxHNsQQ1Wn)