<?php

namespace Tests\Unit;

use App\Entities\StudioClass;
use App\Services\StudioClassService;
use Tests\TestCase;

class StudioClassServiceTest extends TestCase
{
    /**
     * @var StudioClassService
     */
    private $studioClassService;

    /**
     * StudioClassServiceTest constructor
     */
    public function __construct()
    {
        $this->createStudioClassService();

        parent::__construct();
    }

    /**
     * Happy path for creating a class
     *
     * @return void
     */
    public function testCreateAClassSuccess()
    {
        $classObject = ["class_time" => "string", "start_date"=> "string", "end_date"=> "string", "studio_id"=> 0];

        $class = $this->studioClassService->createClass($classObject);

        $this->assertInstanceOf(StudioClass::class, $class);

        $class = $class->toArray(); // for validating fields
        $this->assertArrayHasKey('class_time', $class);
        $this->assertArrayHasKey('start_date', $class);
        $this->assertArrayHasKey('end_date', $class);
        $this->assertArrayHasKey('studio_id', $class);
        $this->assertArrayHasKey('id', $class);
    }

    /**
     * @return void
     */
    protected function createStudioClassService()
    {
        $this->studioClassService = new StudioClassService;
    }
}
