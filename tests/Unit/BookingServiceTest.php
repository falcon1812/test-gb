<?php

namespace Tests\Unit;

use App\Entities\Booking;
use App\Services\BookingService;
use Tests\TestCase;

class BookingServiceTest extends TestCase
{
    private $bookingSerive;


    public function __construct()
    {
        $this->createBookingService();

        parent::__construct();
    }

    /**
     * Happy path for creating a booking
     *
     * @return void
     */
    public function testCreateABookingSuccess()
    {
        $bookingObject = ["class_time" => "string", "start_date"=> "string", "end_date"=> "string", "studio_id"=> 0];

        $booking = $this->bookingSerive->createBooking($bookingObject); // create returns entity

        $this->assertInstanceOf(Booking::class, $booking);

        $booking = $booking->toArray(); // for validating fields
        $this->assertArrayHasKey('user_name', $booking);
        $this->assertArrayHasKey('time', $booking);
        $this->assertArrayHasKey('class_id', $booking);
        $this->assertArrayHasKey('studio_id', $booking);
    }

    /**
     * @return void
     */
    protected function createBookingService()
    {
        $this->bookingSerive = new BookingService;
    }
}
